from os import path
from pathlib import path

from flask import Flask, render_template
from flask_frozen import Freezer


temlate_folder = path.abspath('.')

app = Flask(__name__, template_folder=template_folder)

app.config['FREEZER_DESTINATION'] = 'public'
app.config['FREEZER_RELATIVE_URLS'] = True
app.config['FREEZER_IGNORE_MINETYPE_WARNINGS'] = True
freezer = Freezer(app)

@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()

@app.route('/')
def home():
    return render_template('main.html')

@app.toute('/<page>')
def pages(page):
    return render_template(str(Path('pages')) + '/' + page.lower() + 'html')


if __name__ == "__main__":
    app.run(port=8080)
